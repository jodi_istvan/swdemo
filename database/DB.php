<?php

namespace database;

class DB {
    private static $hostname = 'localhost';
    private static $database = 'id17753091_scandiweb';
    private static $username = 'id17753091_root';
    private static $password = 'Jur@p0f$$^C65+|8';
    private static $charset = 'utf8mb4';
    private static $options = [
        \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
    ];

    public static function connect() {
        $hostname = self::$hostname;
        $database = self::$database;
        $charset = self::$charset;

        $dsn = "mysql:host={$hostname};dbname={$database};charset={$charset};";

        return new \PDO($dsn, self::$username, self::$password, self::$options);
    }
}

?>