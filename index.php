<?php

use app\Controllers\ProductController;

// Files included here will be available to every route 
include_once('utilities/autoloader.php');
include_once('resources/views/view.php');

$requestedURI = $_SERVER['REQUEST_URI'];

if(str_contains($_SERVER['REQUEST_URI'], "?")) {
    $requestedURI = explode("?", $_SERVER['REQUEST_URI'])[0];
}

// Request handler
switch($requestedURI) {
    case "/":
        ProductController::index();
        break;
    case "/add-product":
        ProductController::addProduct();
        break;
    case "/create-product":
        ProductController::create($_REQUEST);
        break;
    case "/mass-delete":
        ProductController::destroy($_REQUEST);
}

?>