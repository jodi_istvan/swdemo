<?php

$errors;
$form_filled;

if(isset($_GET['errors'])) {
    $errors = json_decode($_GET['errors']);
}

if(isset($_GET['form_filled'])) {
    $form_filled = json_decode($_GET['form_filled']);
}

$errorSKU;
$errorName;
$errorPrice;
$errorSize;
$errorWeight;
$errorHeight;
$errorWidth;
$errorLength;

if(isset($errors->SKU)) {
    switch($errors->SKU[0]) {
        case 'required':
            $errorSKU = "Please, enter SKU";
            break;
        case 'text':
            $errorSKU = "Please enter data of type text";
            break;
        case 'exists';
            $errorSKU = "SKU must be unique";
    }
}

if(isset($errors->name)) {
    switch($errors->name[0]) {
        case 'required':
            $errorName =  "Please, enter name";
            break;
        case 'text':
            $errorName = "Please enter data of type text";
            break;
    }
}

if(isset($errors->price)) {
    switch($errors->price[0]) {
        case 'required':
            $errorPrice =  "Please, enter price";
            break;
        case 'number':
            $errorPrice = "Please enter data of type number";
            break;
    }
}

if(isset($errors->size)) {
    switch($errors->size[0]) {
        case 'required':
            $errorSize =  "Please, enter size";
            break;
        case 'number':
            $errorSize = "Please enter data of type number";
            break;
    }
}

if(isset($errors->weight)) {
    switch($errors->weight[0]) {
        case 'required':
            $errorWeight =  "Please, enter weight";
            break;
        case 'number':
            $errorWeight = "Please enter data of type number";
            break;
    }
}

if(isset($errors->height)) {
    switch($errors->height[0]) {
        case 'required':
            $errorHeight =  "Please, enter height";
            break;
        case 'number':
            $errorWeight = "Please enter data of type number";
            break;
    }
}

if(isset($errors->width)) {
    switch($errors->width[0]) {
        case 'required':
            $errorWidth =  "Please, enter width";
            break;
        case 'number':
            $errorWidth = "Please enter data of type number";
            break;
    }
}

if(isset($errors->length)) {
    switch($errors->length[0]) {
        case 'required':
            $errorLength =  "Please, enter length";
            break;
        case 'number':
            $errorLength = "Please enter data of type number";
            break;
    }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="resources/css/style.css">
    <title>Add product</title>
</head>

<body>

    <!-- Nav Begin -->

    <nav class="container-lg">
        <div class="row">
            <div class="col-10">
                <h1>Add product</h1>
            </div>

            <div class="col my-auto">
                <button class="btn btn-primary" type="submit" form="product_form">Save</button>

                <a class="btn btn-secondary text-white" href="/">Cancel</a>
            </div>
        </div>
    </nav>

    <!-- Nav end -->

    <!-- Add product form begin -->

    <form action="create-product" method="POST" id="product_form">
        <div class="container-lg">
            <div class="col-3">
                <div class="row">

                    <label for="sku" class="form-label">SKU </label>
                    <input type="text" name="SKU" id="sku" value=<?php
                        echo (!isset($errorSKU) && isset($form_filled->SKU)) ? $form_filled->SKU : "";
                    ?>><br>
                    <div id="sku-error" class="form-text text-danger">
                        <?php
                        echo (isset($errorSKU)) ? $errorSKU : "";
                        ?>
                    </div>

                    <label for="name" class="form-label">Name </label>
                    <input type="text" name="name" id="name" value=<?php
                        echo (!isset($errorName) && isset($form_filled->name)) ? $form_filled->name : "";
                    ?>><br>
                    <div id="name-error" class="form-text text-danger">
                        <?php
                        echo (isset($errorName)) ? $errorName : "";
                        ?>
                    </div>

                    <label for="price" class="form-label">Price </label>
                    <input type="text" name="price" id="price" value=<?php
                        echo (!isset($errorPrice) && isset($form_filled->price)) ? $form_filled->price : "";
                    ?>><br>
                    <div id="price-error" class="form-text text-danger">
                        <?php
                        echo (isset($errorPrice)) ? $errorPrice : "";
                        ?>
                    </div>

                    <label for="productType" class="form-label">Type Switcher</label>
                    <select type="select" name="productType" id="productType">
                        <option value="dvd"
                        <?php
                            echo (isset($form_filled->productType) && $form_filled->productType == 'dvd') ? "selected" : "";
                        ?>
                        >DVD</option>
                        <option value="book"
                        <?php
                            echo (isset($form_filled->productType) && $form_filled->productType == 'book') ? "selected" : "";
                        ?>
                        >Book</option>
                        <option value="furniture"
                        <?php
                            echo (isset($form_filled->productType) && $form_filled->productType == 'furniture') ? "selected" : "";
                        ?>
                        >Furniture</option>
                    </select>
                    <!-- Special attributes -->

                    <!-- DVD -->
                    <div id="dvd-type">
                        <label for="size" class="form-label">Size (MB) </label>
                        <input type="text" name="size" id="size" value=<?php
                            echo (!isset($errorSize) && isset($form_filled->size)) ? $form_filled->size : "";
                        ?>>
                        <div id="size-error"class="form-text text-danger">
                        <?php
                            echo (isset($errorSize)) ? $errorSize : "";
                        ?>
                        </div>

                        <p class="description">Please provide DVD size in MB.</p>
                    </div>

                    <!-- Book -->
                    <div id="book-type">
                        <label for="weight" class="form-label">Weight (KG) </label>
                        <input type="text" name="weight" id="weight" value=<?php
                            echo (!isset($errorWeight) && isset($form_filled->weight)) ? $form_filled->weight : "";
                        ?>>
                        <div id="weight-error" class="form-text text-danger">
                        <?php
                            echo (isset($errorWeight)) ? $errorWeight : "";
                        ?>
                        </div>

                        <p class="description">Please provide book weight in KG.</p>
                    </div>

                    <!-- Furniture -->
                    <div id="furniture-type">
                        <label for="height" class="form-label">Height (CM) </label>
                        <input type="text" name="height" id="height" value=<?php
                            echo (!isset($errorHeight) && isset($form_filled->height)) ? $form_filled->height : "";
                        ?>>
                        <div id="height-error" class="form-text text-danger">
                        <?php
                            echo (isset($errorHeight)) ? $errorHeight : "";
                        ?>
                        </div>

                        <label for="width" class="form-label">Width (CM) </label>
                        <input type="text" name="width" id="width" value=<?php
                            echo (!isset($errorWidth) && isset($form_filled->width)) ? $form_filled->width : "";
                        ?>>
                        <div id="width-error" class="form-text text-danger">
                        <?php
                            echo (isset($errorWidth)) ? $errorWidth : "";
                        ?>
                        </div>

                        <label for="length" class="form-label">Length (CM) </label>
                        <input type="text" name="length" id="length" value=<?php
                            echo (!isset($errorLength) && isset($form_filled->length)) ? $form_filled->length : "";
                        ?>>
                        <div id="length-error" class="form-text text-danger">
                        <?php
                            echo (isset($errorLength)) ? $errorLength : "";
                        ?>
                        </div>

                        <p class="description">Please provide furniture dimensions in H x W x L format.</p>
                    </div>

                </div>
            </div>
        </div>
    </form>

    <!-- Add product form end -->

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
        crossorigin="anonymous"></script>
    <script src="resources/js/validate.js"></script>
    <script src="resources/js/type-switcher.js"></script>
    
</body>

</html>