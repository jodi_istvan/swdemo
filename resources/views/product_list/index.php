<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="resources/css/style.css">
    <title>Product list</title>
</head>

<body>
    <nav class="container-lg">
        <div class="row">
            <div class="col-10">
                <h1>Product List</h1>
            </div>

            <div class="col my-auto">
                <a class="btn btn-primary" href="add-product">ADD</a>

                <button class="btn btn-danger" type="submit" method="POST" form="delete_products" id="delete-product-btn">MASS DELETE</button>
            </div>
        </div>
    </nav>

    <form action="mass-delete" method="POST" id="delete_products">

        <div class="container-lg">
            <div class="row">
                <?php

                foreach($values as $product) {
                    echo "
                    <div class='col-3'>
                    <div class='border border-1 border-dark' style='padding: 10px; margin: 20px;'>
                        <input type='checkbox' name='products[]' value='{$product['SKU']}|{$product['type']}'
                                class='delete-checkbox'>
                        <div class='container d-flex flex-column align-items-center'>
                            <p>{$product['SKU']}</p>
                            <p>{$product['name']}</p>
                            <p>{$product['price']}</p>
                            <p>{$product['attribute']}</p>
                        </div>
                        </div>
                    </div>
                    <br>";
                }
                
                ?>

            </div>
        </div>
    </form>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
        crossorigin="anonymous"></script>
</body>

</html>