function validate(field, value) {
    let error;

    field.conditions.forEach((condition) => {
        if (condition === 'required') {
            if (value.length === 0) {
                error = "Please, enter " + field.name
                return
            }
        } else if (condition === 'number') {
            if (isNaN(value)) {
                error = "Please, enter data of type number"
                return
            }
        }
    })

    return error;
}

function addValidatorToField(field) {
    let fieldDOM = document.getElementById(field.name);

    if (fieldDOM) {
        fieldDOM.addEventListener('input', (e) => {
            let error = validate(field, e.target.value);
            let errorField = document.getElementById(field.name.toLowerCase() + "-error");

            if (error) {
                errorField.innerHTML = error;
            } else {
                errorField.innerHTML = "";
            }
        });
    }
}

let inputFields = [
    {
        "name": "sku",
        "conditions": [
            "required"
        ]
    },
    {
        "name": "name",
        "conditions": [
            "required"
        ]
    },
    {
        "name": "price",
        "conditions": [
            "required",
            "number"
        ]
    },
    {
        "name": "size",
        "conditions": [
            "required",
            "number"
        ]
    },
    {
        "name": "weight",
        "conditions": [
            "required",
            "number"
        ]
    },
    {
        "name": "height",
        "conditions": [
            "required",
            "number"
        ]
    },
    {
        "name": "width",
        "conditions": [
            "required",
            "number"
        ]
    },
    {
        "name": "length",
        "conditions": [
            "required",
            "number"
        ]
    }
];


inputFields.forEach((field) => {
    addValidatorToField(field)
})
