const typeSwitcher = document.getElementById('productType')

let dvdField = document.getElementById('dvd-type')
let bookField = document.getElementById('book-type')
let furnitureField = document.getElementById('furniture-type')

const dvdFieldHTML = dvdField.innerHTML
const bookFieldHTML = bookField.innerHTML
const furnitureFieldHTML = furnitureField.innerHTML

function switchAttributeField(type) {
    switch (type) {
        case 'dvd':
            dvdField.innerHTML = dvdFieldHTML
            bookField.innerHTML = ""
            furnitureField.innerHTML = ""

            let dvdInput = {
                "name": "size",
                "conditions": [
                    "required",
                    "number"
                ]
            }

            addValidatorToField(dvdInput);

            break
        case 'book':
            dvdField.innerHTML = ""
            bookField.innerHTML = bookFieldHTML
            furnitureField.innerHTML = ""

            let bookInput = {
                "name": "weight",
                "conditions": [
                    "required",
                    "number"
                ]
            }

            addValidatorToField(bookInput);

            break
        case 'furniture':
            dvdField.innerHTML = ""
            bookField.innerHTML = ""
            furnitureField.innerHTML = furnitureFieldHTML

            let fields = [
                {
                    "name": "height",
                    "conditions": [
                        "required",
                        "number"
                    ]
                },
                {
                    "name": "width",
                    "conditions": [
                        "required",
                        "number"
                    ]
                },
                {
                    "name": "length",
                    "conditions": [
                        "required",
                        "number"
                    ]
                }
            ]

            fields.forEach((field) => {
                addValidatorToField(field)
            })

            break
    }
}

switchAttributeField(typeSwitcher.value)

typeSwitcher.addEventListener('change', (e) => {
    const type = e.target.value

    switchAttributeField(type)
})