<?php

namespace app\Models;
use app\Models\ProductsModel;

class Furniture extends ProductsModel {
    protected static $table = 'furnitures';

    public $height;
    public $width;
    public $length; 

    public function save() {
        $values = [
            'SKU' => $this->SKU,
            'name' => $this->name,
            'price' => $this->price,
            'height' => $this->height,
            'width' => $this->width,
            'length' => $this->length
        ];

        self::insert($values)->runQuery();
    }
}

?>