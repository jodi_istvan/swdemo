<?php

namespace app\Models;
use app\Models\BaseModel;

abstract class ProductsModel extends BaseModel {
    public $SKU;
    public $name;
    public $price;
    public $attributes;

    public static function get($SKU) {
        $product = self::select(['*'])->where('SKU', '=', $SKU)->runQuery();

        return $product;
    }

    public static function destroy($SKU) {
        $product = self::delete()->where('SKU', '=', $SKU)->runQuery();
    }

    abstract public function save();
}

?>