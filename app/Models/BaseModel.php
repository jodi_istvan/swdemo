<?php

namespace app\Models;
use database\DB;

class BaseModel {
    protected static $table;
    private static $sql = '';
    private static $bindings = [];

    public function __construct() {}

    // Statically callable query builder using method chaining 
    public static function raw(string $query, ?array $bindings) {
        self::$sql = $query;
        self::$bindings = $bindings;

        return new BaseModel();
    }

    public static function select(array $columns) {
        $_columns = '';

        foreach($columns as $column) {
            $_columns .= $column . ', ';
        }
        $_columns = substr($_columns, 0, -2);

        $table = self::$table;
        self::$sql .= "SELECT {$_columns} FROM " . static::$table;

        return new BaseModel();
    }

    public static function insert(array $values) {
        self::$bindings = array_merge(self::$bindings, $values);

        $_keys = "";
        $_values = "";

        foreach($values as $key => $value) {
            $_keys .= $key . ', ';
            $_values .= ":{$key}, ";
        }

        $_keys = substr($_keys, 0, -2);
        $_values = substr($_values, 0, -2);

        self::$sql = "INSERT INTO " . static::$table . "({$_keys}) VALUES ({$_values})";

        return new BaseModel();
    }

    public static function update(array $values) {
        self::$bindings = array_merge(self::$bindings, $values);

        $_setValues = "";

        foreach($values as $key => $value) {
            $_setValues .= "{$key}=:{$key}, ";
        }

        $_setValues = substr($_setValues, 0, -2);

        self::$sql = "UPDATE " . static::$table . " SET {$_setValues}";

        return new BaseModel();
    }

    public static function delete() {
        self::$sql = "DELETE FROM " . static::$table;

        return new BaseModel();
    }

    public function where(string $colValue, string $operator = '=', $compareValue) {
        self::$sql .=  " WHERE " . $colValue . $operator . ":".$colValue;
        self::$bindings = array_merge(self::$bindings, [$colValue => $compareValue]);

        return new BaseModel();
    }

    public function and(string $colValue, string $operator = '=', $compareValue) {
        self::$sql .=  " AND " . $colValue . " " . $operator . " :".$colValue;
        self::$bindings = array_merge(self::$bindings, [$colValue => $compareValue]);

        return new BaseModel();
    }

    public function limit(int $limit) {
        self::$sql .= " LIMIT {$limit}";

        return new BaseModel();
    }

    public function runQuery() {
        self::$sql .= ";";
        $result = [];

        $pdo = DB::connect();
        
        if(self::$bindings) {
            $stmt = $pdo->prepare(self::$sql);
            $stmt->execute(self::$bindings);
        }else {
            $stmt = $pdo->query(self::$sql);
        }
    
        while($row = $stmt->fetch()) {
            array_push($result, $row);
        }

        self::$sql = '';
        self::$bindings = [];

        return $result;
    }
}

?>