<?php

namespace app\Models;
use app\Models\ProductsModel;

class Dvd extends ProductsModel {
    protected static $table = 'dvds';

    public $size;

    public function save() {
        $values = [
            'SKU' => $this->SKU,
            'name' => $this->name,
            'price' => $this->price,
            'size' => $this->size
        ];

        self::insert($values)->runQuery();
    }
}

?>