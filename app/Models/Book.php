<?php

namespace app\Models;
use app\Models\ProductsModel;

class Book extends ProductsModel {
    protected static $table = 'books';

    public $weight;

    public function save() {
        $values = [
            'SKU' => $this->SKU,
            'name' => $this->name,
            'price' => $this->price,
            'weight' => $this->weight
        ];

        self::insert($values)->runQuery();
    }
}

?>