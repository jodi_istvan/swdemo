<?php

namespace app\Controllers;
use app\Models\Book;
use app\Controllers\ProductController;
use utilities\Validator;

class BookController {
    public static function save($request) {
        $validator = new Validator($request);

        $errors = $validator->validate([
            'SKU' => 'required|text',
            'name' => 'required|text',
            'price' => 'required|number',
            'weight' => 'required|number'
        ]);

        if(ProductController::existsInDB($request['SKU'])) {
            if(isset($errors['SKU'])) {
                array_push($errors['SKU'], 'exists');
            }else {
                $errors['SKU'] = ['exists'];
            }
        }
        
        if($errors) {
            $errors = json_encode($errors);

            throw new \Exception($errors);
        }

        $book = new Book();

        $book->SKU = $request['SKU'];
        $book->name = $request['name'];
        $book->price = $request['price'];
        $book->weight = $request['weight'];

        $book->save();
    }

    public static function destroy($SKU) {
        Book::delete()->where('SKU', '=', $SKU)->runQuery();
    }
}

?>