<?php

namespace app\Controllers;
use app\Models\Furniture;
use app\Controllers\ProductController;
use utilities\Validator;

class FurnitureController {
    public static function save($request) {
        $validator = new Validator($request);

        $errors = $validator->validate([
            'SKU' => 'required|text',
            'name' => 'required|text',
            'price' => 'required|number',
            'height' => 'required|number',
            'width' => 'required|number',
            'length' => 'required|number'
        ]);
        
        if(ProductController::existsInDB($request['SKU'])) {
            if(isset($errors['SKU'])) {
                array_push($errors['SKU'], 'exists');
            }else {
                $errors['SKU'] = ['exists'];
            }
        }

        if($errors) {
            $errors = json_encode($errors);

            throw new \Exception($errors);
        }

        $furniture = new Furniture();

        $furniture->SKU = $request['SKU'];
        $furniture->name = $request['name'];
        $furniture->price = $request['price'];
        $furniture->height = $request['height'];
        $furniture->width = $request['width'];
        $furniture->length = $request['length'];

        $furniture->save();
    }

    public static function destroy($SKU) {
        Furniture::delete()->where('SKU', '=', $SKU)->runQuery();
    }
}

?>