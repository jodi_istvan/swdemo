<?php

namespace app\Controllers;
use app\Models\Dvd;
use app\Controllers\ProductController;
use utilities\Validator;

class DvdController {
    public static function save($request) {
        $validator = new Validator($request);

        $errors = $validator->validate([
            'SKU' => 'required|text',
            'name' => 'required|text',
            'price' => 'required|number',
            'size' => 'required|number'
        ]);

        if(ProductController::existsInDB($request['SKU'])) {
            if(isset($errors['SKU'])) {
                array_push($errors['SKU'], 'exists');
            }else {
                $errors['SKU'] = ['exists'];
            }
        }
        
        if($errors) {
            $errors = json_encode($errors);

            throw new \Exception($errors);
        }

        $dvd = new Dvd();

        $dvd->SKU = $request['SKU'];
        $dvd->name = $request['name'];
        $dvd->price = $request['price'];
        $dvd->size = $request['size'];

        $dvd->save();
    }

    public static function destroy($SKU) {
        Dvd::delete()->where('SKU', '=', $SKU)->runQuery();
    }
}

?>