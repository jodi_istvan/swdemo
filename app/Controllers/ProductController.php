<?php

namespace app\Controllers;
use app\Models\Book;
use app\Models\Dvd;
use app\Models\Furniture;
use app\Models\BaseModel;

abstract class ProductController {
    public static function existsInDB($SKU) {
        $types = BaseModel::raw("SELECT * FROM product_types", NULL)->runQuery();

        foreach($types as $type) {
            $tableName = $type['name'] . 's';

            $exists = BaseModel::raw("SELECT 1 FROM {$tableName} WHERE SKU=:SKU", [
                "SKU" => $SKU
            ])->runQuery();

            if($exists) {
                return true;
            }
        }

        return false;
    }

    public static function index() {
        $books = Book::select(['*'])->runQuery();
        $dvds = Dvd::select(['*'])->runQuery();
        $furnitures = Furniture::select(['*'])->runQuery();

        // Format product details before passing them to the view
        foreach ($books as $i => $book) {
            $books[$i]["price"] .= " $";
            $books[$i]["type"] = "book";
            $books[$i]["attribute"] = "Weight " . $books[$i]["weight"] . " KG";
        }
        foreach ($dvds as $i => $dvd) {
            $dvds[$i]["price"] .= " $";
            $dvds[$i]["type"] = "dvd";
            $dvds[$i]["attribute"] = "Size ". $dvds[$i]["size"] . " MB";
        }
        foreach ($furnitures as $i => $furniture) {
            $furnitures[$i]["price"] .= " $";
            $furnitures[$i]["type"] = "furniture";
            $furnitures[$i]["attribute"] = "Dimensions ". $furnitures[$i]["height"] . "x" . $furnitures[$i]["width"] . "x" . $furnitures[$i]["length"];
        }

        $products = array_merge($books, $dvds, $furnitures);

        // Sort products by SKU
        usort($products, function ($prod1, $prod2) {
            return $prod1['SKU'] <=> $prod2['SKU'];
        });

        return view('product_list/index', $products);
    }

    public static function addProduct() {
        return view('add_product/index', NULL);
    }

    public static function create($request) {
        $productControllerClass = '\app\Controllers\\' . ucfirst($request['productType']) . "Controller";

        try {
            $productControllerClass::save($request);

            header("Location: /");
        }catch(\Exception $e) {
            $errors = $e->getMessage();

            $form_filled = json_encode($request);

            header("Location: /add-product?errors={$errors}&form_filled={$form_filled}");
        }
    }

    public static function destroy($request) {
        foreach($request['products'] as $product) {
            $productControllerClass = '\app\Controllers\\' . ucfirst(explode('|', $product)[1]) . "Controller";
            
            $productControllerClass::destroy(ucfirst(explode('|', $product)[0]));

            header('Location: /');
        }
    }
} 

?>