<?php

namespace utilities;

class Validator {
    private $request;
    private $errors;

    public function __construct(array $request) {
        $this->request = $request;
        $this->errors = [];
    }

    private function required($value) {
        return strlen($value) > 0;
    }

    private function text($value) {
        if(strlen($value) > 0) {
            return (string) $value != 0;
        }

        return false;
    }

    private function number($value) {
        if(strlen($value) > 0) {
            return is_numeric($value);
        }

        return false;
    }

    public function validate($rules) {
        $errors = [];

        foreach($rules as $field => $conditions) {
            $conditions = explode('|', $conditions);

            foreach($conditions as $condition) {
                // $this->$condition ==> call member function by string
                if(!$this->$condition($this->request[$field])) {
                    if(!isset($errors[$field])) {
                        $errors[$field] = [$condition];
                    }else {
                        array_push($errors[$field], $condition);        
                    }
                }
            }
        }

        return $errors;
    }
}

?>